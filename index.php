<!DOCTYPE html>
<<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sample Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
</head>
<body>

    <h1>Login Form</h1>

    <form action="process.php" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" class="input" name="user" placeholder="username">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Password</label>
            <input type="password" class="input" name="pass" placeholder="password">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" value"Login">
        </div>

    </form>

</body>
</html>                        
